import { RouterModule, Routes } from '@angular/router';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProgressComponent } from './progress/progress.component';
import { Graficas1Component } from './graficas1/graficas1.component';

const PAGESROUTES: Routes = [
    {
        path: '', component: PagesComponent, children: [
            // avec l'autre router outlet secondaire.
            { path: 'dashboard', component: DashboardComponent },
            { path: 'progress', component: ProgressComponent },
            { path: 'graficas1', component: Graficas1Component },
            { path: '', redirectTo: '/dashboard', pathMatch: 'full'},
        ]
    },
];

// forRoot  => pour les routes principales
// forChild => pour les routes dans les routes
//          => des router-outlet dans dautres router outlet
export const PAGES_ROUTES = RouterModule.forChild(PAGESROUTES);
